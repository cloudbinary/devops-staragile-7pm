Getting Started with terraform :

https://www.terraform.io/

STEP-1 : Download, Install & Configure terraform on Windows 

https://www.terraform.io/downloads

Way Of Installation : WINDOWS BINARY DOWNLOAD

terraform_1.3.1

https://releases.hashicorp.com/terraform/1.3.1/terraform_1.3.1_windows_amd64.zip

STEP-2 : Unzip the terraform Software 

Unzip the file:
terraform_1.3.1_windows_amd64.zip

terraform_1.3.1_windows_amd64/terraform.exe 

STEP-3 : Copy terraform file unto C:/Program Files/ 

Note: By Creating a Folder i.e. Terraform

Copy terraform file(i.e. terraform.exe) from downloads to C:/Program Files/Terraform/terraform.exe

STEP-4 : Setup Environment Variable for terraform

System variables:
Start-->System-->Advanced System Settings-->Environment Variables-->System variables-->Select & Click on Edit "path"

C:/Program Files/Terraform/

User variables:
Start-->System-->Advanced System Settings-->Environment Variables-->User variables-->Select & Click on Edit "path"

C:/Program Files/Terraform/

STEP-5 : Close all the CMD/PowerShell/Gitbash and restart your PC and open CMD/PowerShell/Gitbash 

Varify the terraform version :

$ terraform --version 
