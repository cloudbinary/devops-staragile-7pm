cloudbinary@Clouds-MacBook-Pro DAY-9_20221002 % packer --version
1.6.0

cloudbinary@Clouds-MacBook-Pro DAY-9_20221002 % packer   

Usage: packer [--version] [--help] <command> [<args>]

Available commands are:
    build       build image(s) from template
    console     creates a console for testing variable interpolation
    fix         fixes templates from old versions of packer
    inspect     see components of a template
    validate    check that a template is valid
    version     Prints the Packer version

