# Build an Image

packer.json :
    3 Blocks:
        1. variables
        2. builders
        3. provisioners

{
    "variables":{

    },
    "builders": [
        {

        }
    ],
    "provisioners": [
        {

        }
    ]

}


        {
            "type": "ansible-local",
            "playbook_file": "./tomcat-setup.yml",
            "extra_arguments": [
                "-vvvv"
            ]
        },
        {
            "type": "shell",
            "inline": [
                "sudo aws s3 cp s3://staragiledevops-weekend/opswork.war /usr/share/tomcat/webapps/",
                "sudo ls -lrt /usr/share/tomcat/webapps/"
            ]
        }
        