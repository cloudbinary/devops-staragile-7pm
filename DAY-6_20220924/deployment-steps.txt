

STEP-1 :  Create deployment.yml

File Name : deployment.yml

STEP-2 : Create Service and Deploy:

File Name : nginx-service.yaml

kubectl create service nodeport nginx --tcp=80:80

kubectl apply -f deployment.yml

STEP-3 : Update the deployment with new version

File Name : deployment-update.yaml

kubectl apply -f deployment-update.yaml

STEP-4 : Scale the deployment

File Name : deployment-scale.yaml

kubectl apply -f  deployment-scale.yaml 

STEP-5 : Deleting a deployment 

kubectl delete deployment nginx-deployment
