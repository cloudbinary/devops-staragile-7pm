
1. Continuous Monitoring (SelfHosted(IaaS) or CloudHosted(SaaS)):
    - Infra
    - Apps

2. ITSM (Information Technology Service Management) : [ i.e. ServiceNow ]

Reports & Dashboards :
    - Change Management
    - Problem Management
    - Incident Management :
        - System Analysis 
        - Troubleshooting

Service Improvement:    
    - Policy and Procedure Management
    - Asset Management
    - Project Management :
        - Contract Management :
            - Cloud Services 
            - Vendor Management

Service Strategy :
    - Service Catalog
    - Service Desk 
    - Knowledge Management

Alert Management :
    - Incident will be created when there is problem 

3. Infrastructure Provisioning :
    - GUI / Programmatic 

4. Build & Deployment :
    - Creating of CI/CD Pipelines :
        - Cloud Native CI/CD tools or Combination of 3rd party 
    - End To End Documentation 

5. Cost Optimisation :
    - Creating Standard Tags on Infrastructure Provisioning you can save lot of costs 
    - Alert condition or you may write python code 

6. Disaster Recovery 

7. Migrations 

8. Automation :
    - Shell / Powershell
    - Ansible 
    - Python / Java etc ....

9. Documentation

10. SDLC : Agile & Scrum Framework 

Agile : 
    - 4 Values
    - 12 Priciples 

Scrum Framework :
    - Roles :
        - Product owner
        - Scrum Master 
        - Scrum Team(DevOps Team): --> DevSecOps --> Combo-Shaped IT Professional [4 -12] 
            - Dev 
            - QA 
            - DB Developer
            - DB Admin 
            - Windows Admin / Linux / Network Engineer / Security Engineer
            - Automation Engineer
            - Ops Team(L1, L2 & L3)
            
    - Rituals :
        - Sprint Planning (End The Sprint and Start New Sprint )  :  2 Hrs per Iteration
        - Daily Scrum       : 15 Minutes
        - Product Backlog Refinement : 60 Minutes in a Iteration
        - Sprint Backlog Refinement : 60 Minutes in a Iteration
        - Sprint Review : 60 Minutes in a Iteration
        - Sprint Restrospection : 60 Minutes in a Iteration

    - Artifacts :
        - EPIC
            - Features 
            - Bugs 
            - User Stories 
                - Tasks

Attendees: Development team, scrum master, product owner

When: At the beginning of a sprint.

Duration: Usually around one hour per week of iteration. e.g. a two-week sprint kicks off with a two-hour planning meeting.

Agile Framework: Scrum. (Kanban teams also plan, of course, but they are not on a fixed iteration schedule with formal sprint planning)

Purpose: Sprint planning sets up the entire team for success throughout the sprint. Coming into the meeting, the product owner will have a prioritized product backlog. They discuss each item with the development team, and the group collectively estimates the effort involved. The development team will then make a sprint forecast outlining how much work the team can complete from the product backlog. That body of work then becomes the sprint backlog.



Create MySQL :

Provisioning : IaC Tool : Terraform 

Connect TO DB 

Versions :

Create Schema 
Create a Table 
Insert some sample records to the table 
Query them 
Alter them 
Truncate them 
Delete table 
delete Schema

