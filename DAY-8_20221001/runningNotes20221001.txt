Agenda :


Module 9 - Configuration Management using Chef [Completed]
Topics:
•	Chef Fundamentals
•	Chef Architecture & Components – Server, Workstation and Nodes 
•	Chef Resources
•	Recipes and Cookbooks
•	Chef Resources 
•	Using AWS OpsWorks

Practical’s to be covered: 
•	Creating Stack using AWS OpsWorks

Module 11 - Continuous Monitoring using Nagios [Completed]
Topics: 
•	Introduction to Nagios
•	Nagios Plugins
•	Nagios Objects
•	Nagios Commands & Nagios Notifications

Practicals to be covered: 
•	Installing Nagios
•	Monitoring different servers using Nagios


Hashicorp Packer :
    - Build Automated Machine Images
    - Create identical machine images for multiple platforms from a single source configuration.

    - Creating Golden Images :
        Deploy a Website : Java, Maven, --> Tomcat 
            - Linux :
                - Prerequisites : java, maven & tomcat 
                    - tomcat/webapps/cloudops-1.0.0.war 
                    - Code has been deployed unto the environment[To Access a website]

    - AWS --> Create temporary Linux Server :
        Deploy a Website : Java, Maven, --> Tomcat 
            - Linux :
                - Prerequisites : java, maven & tomcat 
                    - tomcat/webapps/cloudops-1.0.0.war 
                    - Code has been deployed unto the environment[To Access a website]
    - Create Golden AMI from temporary ec2 instance & terminate temporary ec2 instance
    - Golden AMI is input to your Terraform/IAC ---> EC2 instance[]

Hashicorp Terraform :


Q?